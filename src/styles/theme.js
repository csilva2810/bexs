const fontSizes = [16, 19];

const fontWeights = {
  light: 300,
  regular: 400,
  semibold: 500,
  bold: 600,
};

const space = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 90, 120];

const colors = {
  white: '#FFFFFF',
  black: '#000000',
  text: '#3C3C3C',
  primary: '#DE4B4B',
  error: '#EB5757',
  placeholder: '#C9C9C9',
  border: '#C9C9C9',
};

const breakpoints = ['1024px'];

export default {
  fontSizes,
  fontWeights,
  space,
  colors,
  breakpoints,
};

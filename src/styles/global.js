import { createGlobalStyle } from 'styled-components/macro';

import reset from 'styled-reset';

const GlobalStyles = createGlobalStyle`
  ${reset};

  *, *::before, *::after {
    box-sizing: border-box;
  }

  html, body {
    margin: 0;
    padding: 0;
    height: 100%;
    font-family: 'Verdana', sans-serif;
    font-size: 16px;
    line-height: 20px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  #root {
    height: 100%;
  }

  strong {
    font-weight: bold;
  }

  a {
    text-decoration: none;
  }

  input,
  button {
    font-size: 1rem;
    font-family: 'Verdana', sans-serif;
  }

  button {
    background-color: transparent;
    border: none;
    outline: none;
    padding: 0;
    cursor: pointer;
    -webkit-tap-highlight-color: transparent;
    user-select: none;
  }
`;

export default GlobalStyles;

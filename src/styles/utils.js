import { css } from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

export const mediaQuery = (styles, breakpoint = 0) => {
  return css`
    @media screen and (min-width: ${themeGet(`breakpoints.${breakpoint}`)}) {
      ${styles}
    }
  `;
};

import React, { memo } from 'react';

import PaymentForm from './components/Payment';

function App() {
  return <PaymentForm />;
}

export default memo(App);

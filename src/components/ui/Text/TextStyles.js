import { memo } from 'react';
import styled from 'styled-components/macro';
import { color, variant, typography } from 'styled-system';

const textTransform = (props) => {
  if (props.textTransform) {
    return `text-transform: ${props.textTransform}`;
  }

  return '';
};

const variants = variant({
  variants: {
    body1: {
      fontSize: '16px',
    },
    body2: {
      fontSize: '13px',
    },
  },
});

const Text = styled.p`
  font-family: 'Verdana', sans-serif;
  letter-spacing: -0.01px;
  line-height: 20px;

  ${variants};
  ${color};
  ${typography};
  ${textTransform};
`;

Text.defaultProps = {
  variant: 'body1',
};

export default memo(Text);

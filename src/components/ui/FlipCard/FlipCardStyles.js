import styled from 'styled-components/macro';

const fullSize = `
  width: 100%;
  height: 100%;
`;

export const FlipCard = styled.div`
  ${fullSize}
  position: relative;
  perspective: 600px;
`;
export const Container = styled.div`
  ${fullSize}
  position: relative;
  transform-style: preserve-3d;
  transition: transform 0.6s;

  ${(props) => props.isFlipped && 'transform: rotateY(180deg);'}
`;

export const Face = styled.div`
  ${fullSize}
  position: absolute;
  backface-visibility: hidden;
  transition: transform 1s;

  ${(props) => props.isBack && 'transform: rotateY(180deg);'}
`;

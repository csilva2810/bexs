import React, { memo } from 'react';

import * as Styles from './FlipCardStyles';

const FlipCard = ({ isFlipped = false, front = <div />, back = <div /> }) => (
  <Styles.FlipCard>
    <Styles.Container isFlipped={isFlipped}>
      <Styles.Face>{front}</Styles.Face>
      <Styles.Face isBack>{back}</Styles.Face>
    </Styles.Container>
  </Styles.FlipCard>
);

export default memo(FlipCard);

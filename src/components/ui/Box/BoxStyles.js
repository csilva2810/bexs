import { memo } from 'react';
import styled from 'styled-components/macro';
import { space, layout, color, flexbox } from 'styled-system';

const Box = styled('div')(flexbox, space, layout, color);

Box.defaultProps = {
  display: 'flex',
  flex: '0 0 auto',
};

export default memo(Box);

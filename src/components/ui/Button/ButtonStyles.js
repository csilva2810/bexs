import { memo } from 'react';
import styled from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 51px;
  padding: ${themeGet('space.2')}px;
  color: ${themeGet('colors.white')};
  background-color: ${themeGet('colors.primary')};
  text-transform: uppercase;
  text-align: center;
  font-weight: ${themeGet('fontWeights.semibold')};
  font-family: 'SF Pro Text', Verdana, sans-serif;
  font-size: 17px;
  line-height: 22px;
  letter-spacing: -0.01px;
  border-radius: 10px;

  ${(props) => props.fullWidth && 'width: 100%'};
`;

export default memo(Button);

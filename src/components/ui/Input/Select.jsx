import React, { memo } from 'react';

import { useFloatLabel } from './useFloatLabel';
import * as Styles from './InputStyles';

const Select = ({ items, label, error, ...attrs }) => {
  const { containerRef, inputRef } = useFloatLabel();

  return (
    <Styles.Container>
      <Styles.InputContainer ref={containerRef}>
        <Styles.Label htmlFor={attrs.id} noIteract={true}>
          {label}
        </Styles.Label>
        <Styles.Select error={Boolean(error)} ref={inputRef} {...attrs}>
          {items}
        </Styles.Select>
      </Styles.InputContainer>

      {error && <Styles.Error>{error}</Styles.Error>}
    </Styles.Container>
  );
};

export default memo(Select);

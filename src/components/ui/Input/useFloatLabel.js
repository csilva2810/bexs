import { useRef, useCallback, useEffect } from 'react';

const IS_ACTIVE = 'is-active';

export const useFloatLabel = () => {
  const containerRef = useRef();
  const inputRef = useRef();

  const setIsActive = useCallback((isActive) => {
    const action = isActive ? 'add' : 'remove';

    containerRef.current.classList[action](IS_ACTIVE);
  }, []);

  const handleFocus = useCallback(() => {
    setIsActive(true);
  }, [setIsActive]);

  const handleBlur = useCallback(
    (e) => {
      if (e.target.value) {
        return;
      }

      setIsActive(false);
    },
    [setIsActive],
  );

  useEffect(() => {
    const input = inputRef.current;

    if (input.value) {
      setIsActive(true);
    }

    input.addEventListener('blur', handleBlur);
    input.addEventListener('focus', handleFocus);

    return () => {
      input.removeEventListener('blur', handleBlur);
      input.removeEventListener('focus', handleFocus);
    };
  }, [setIsActive, handleBlur, handleFocus]);

  return {
    containerRef,
    inputRef,
  };
};

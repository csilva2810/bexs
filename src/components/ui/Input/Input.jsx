import React, { useEffect, memo } from 'react';
import VMasker from 'vanilla-masker';

import { useFloatLabel } from './useFloatLabel';
import * as Styles from './InputStyles';

const Input = ({ mask, label, error, ...attrs }) => {
  const { inputRef, containerRef } = useFloatLabel();

  useEffect(() => {
    const input = inputRef.current;

    if (mask) {
      VMasker(input).maskPattern(mask);
    }

    return () => {
      if (mask) {
        VMasker(input).unMask();
      }
    };
  }, [mask, inputRef]);

  return (
    <Styles.Container>
      <Styles.InputContainer ref={containerRef}>
        <Styles.Label htmlFor={attrs.id}>{label}</Styles.Label>
        <Styles.Input error={Boolean(error)} ref={inputRef} {...attrs} />
      </Styles.InputContainer>

      {error && <Styles.Error>{error}</Styles.Error>}
    </Styles.Container>
  );
};

export default memo(Input);

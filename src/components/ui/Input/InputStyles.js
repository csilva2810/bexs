import styled, { css } from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

import selectArrow from '../../../assets/select-arrow.svg';

import { mediaQuery } from '../../../styles/utils';

export const Container = styled.div`
  display: block;
  width: 100%;
`;

export const InputContainer = styled.div`
  display: block;
  position: relative;
  height: 32px;
`;

const labelInteract = (props) => props.noIteract && 'pointer-events: none;';
export const Label = styled.label`
  position: absolute;
  top: 6px;
  left: 0;
  color: ${themeGet('colors.placeholder')};
  font-size: 17px;
  transition: 0.3s;
  transform-origin: left;
  will-change: transform;

  .is-active & {
    font-size: 13px;
    transform: translate3D(0, calc(-100% + -5px), 0);
  }

  ${labelInteract}
`;

const inputError = (props) =>
  props.error &&
  css`
    border-color: ${themeGet('colors.error')} !important;
  `;
export const Input = styled.input`
  display: block;
  padding: 0;
  width: 100%;
  height: 100%;
  font-size: 17px;
  background-color: transparent;
  color: ${themeGet('colors.text')};
  outline: none;
  border: none;
  border-bottom: 1px solid ${themeGet('colors.border')};
  transition: 0.3s;

  &:focus {
    border-color: ${themeGet('colors.text')};
  }

  ${inputError};
`;

export const Error = styled.p`
  font-size: 11px;
  color: ${themeGet('colors.error')};
  height: 0;
  overflow-y: visible;

  ${mediaQuery(`
    font-size: 13px;
    margin-top: ${themeGet('space.1')}px;
  `)}
`;

export const Select = styled(Input).attrs(() => ({ as: 'select' }))`
  background-image: url(${selectArrow});
  background-position: center right;
  background-repeat: no-repeat;
  border-radius: 0;
  appearance: none;
`;

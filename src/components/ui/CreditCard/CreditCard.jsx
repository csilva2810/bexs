import React, { memo, useMemo } from 'react';

import Box from '../Box';
import FlipCard from '../FlipCard';

import * as Styles from './CreditCardStyles';

const CreditCard = ({
  flag = '',
  numbers = [],
  name = '',
  validThru = '',
  cvv = '',
  isFlipped = false,
}) => {
  const front = useMemo(
    () => (
      <Styles.Card changeBackground={Boolean(flag)}>
        <Box
          px={[3, 6]}
          height="100%"
          flexDirection="column"
          justifyContent="space-around"
        >
          <Box height="18px">
            {flag && <Styles.Flag src={flag} alt="bandeira do cartão" />}
          </Box>

          <Box
            mt={1}
            alignItems="center"
            justifyContent="space-between"
            data-testid="card-number"
          >
            {numbers.map((numbers, i) => (
              <Styles.Text
                key={numbers + i}
                variant="subtitle1"
                letterSpacing="3px"
              >
                {numbers}
              </Styles.Text>
            ))}
          </Box>

          <Box mb={2} alignItems="center" justifyContent="space-between">
            <Styles.Text
              textTransform="uppercase"
              variant="caption"
              data-testid="card-name"
            >
              {name || 'nome do titular'}
            </Styles.Text>
            <Styles.Text variant="caption" data-testid="card-validThru">
              {validThru || '00/00'}
            </Styles.Text>
          </Box>
        </Box>
      </Styles.Card>
    ),
    [flag, name, numbers, validThru],
  );

  const back = useMemo(
    () => (
      <Styles.Card isBack changeBackground={Boolean(flag)}>
        <span className="cvv" data-testid="card-cvv">
          {cvv}
        </span>
      </Styles.Card>
    ),
    [cvv, flag],
  );

  return (
    <Styles.Container>
      <FlipCard isFlipped={isFlipped} front={front} back={back} />
    </Styles.Container>
  );
};

export default memo(CreditCard);

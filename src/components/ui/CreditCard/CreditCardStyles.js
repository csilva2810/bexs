import styled, { css } from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

import { mediaQuery } from '../../../styles/utils';

import cardFrontBlue from '../../../assets/card-front-blue.svg';
import cardFrontGray from '../../../assets/card-front-gray.svg';
import cardBackGray from '../../../assets/card-back-gray.svg';
import cardBackBlue from '../../../assets/card-back-blue.svg';

import BaseText from '../Text';

// preloading the image to have it ready to make the transition
// between gray and blue backgrounds
new Image().src = cardFrontBlue;
new Image().src = cardBackBlue;

export const Container = styled.div`
  position: relative;
  width: 100%;
  height: 172px;

  ${mediaQuery(`
    width: 365px;
    height: 224px;
  `)}
`;

const cardBackground = ({ isBack, changeBackground }) => {
  if (isBack) {
    if (changeBackground) {
      return `background-image: url(${cardBackBlue});`;
    }

    return `background-image: url(${cardBackGray});`;
  }

  if (changeBackground) {
    return `background-image: url(${cardFrontBlue});`;
  }

  return `background-image: url(${cardFrontGray});`;
};

export const Card = styled.div`
  width: 100%;
  height: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  ${cardBackground}
  box-shadow: 0 15px 12px -10px rgba(0, 0, 0, 0.35);
  transition: background 0.3s;

  .cvv {
    position: absolute;
    top: 48%;
    left: 48%;
    color: ${themeGet('colors.text')};
    font-size: 17px;
  }
`;

export const Flag = styled.img`
  width: auto;
  height: 100%;
`;

export const Text = styled(BaseText)`
  font-family: 'SF Pro Text', Verdana, sans-serif;
  text-shadow: 0px 1px 2px #000000b3;
  font-size: 19px;

  ${mediaQuery(`
    font-size: 24px;
    letter-spacing: 3px;
  `)}

  ${(props) =>
    props.variant === 'caption' &&
    css`
      font-size: 12px;

      ${mediaQuery(`
        font-size: 16px;
        letter-spacing: 0px;
      `)}
    `}
`;

import styled, { css } from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

import { ReactComponent as CheckIcon } from '../../../assets/check-icon.svg';

export const Step = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 22px;
  height: 22px;
  border-radius: 50%;
  border: 1px solid ${themeGet('colors.primary')};
  color: ${themeGet('colors.primary')};
  font-weight: bold;
  font-size: 12px;

  ${(props) =>
    props.isDone &&
    css`
      color: white;
      background-color: ${themeGet('colors.primary')};
    `}
`;

export const Label = styled.div`
  margin-left: ${themeGet('space.2')}px;
  color: ${themeGet('colors.primary')};
  font-size: 13px;
`;

export const Check = styled(CheckIcon)`
  width: 14px;
  fill: white;
`;

import React, { memo } from 'react';

import Box from '../Box';
import Chevron from '../Chevron';

import * as Styles from './BreadcrumbStyles';

const Breadcrumb = ({ items }) => (
  <Box alignItems="center">
    {items.map((item, index) => {
      const { label, isDone } = item;
      const isLast = index === items.length - 1;

      return (
        <Box alignItems="center" key={label + index}>
          <Styles.Step isDone={isDone}>
            {isDone ? <Styles.Check /> : index + 1}
          </Styles.Step>

          <Styles.Label>{label}</Styles.Label>

          {!isLast && (
            <Box mx={5}>
              <Chevron direction="right" size="12px" color="primary" />
            </Box>
          )}
        </Box>
      );
    })}
  </Box>
);

export default memo(Breadcrumb);

import { memo } from 'react';
import styled from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

import { ReactComponent as ChevronIcon } from '../../../assets/chevron.svg';

const direction = (props) => {
  if (props.direction === 'left') {
    return `
      transform: rotate(180deg);
    `;
  }

  return '';
};

const size = (props) => props.size || '21px';
const color = (props) =>
  props.color ? themeGet(`colors.${props.color}`) : 'white';

const Chevron = styled(ChevronIcon)`
  width: ${size};
  fill: ${color};
  ${direction};
`;

Chevron.defaultProps = {
  direction: 'right',
};

export default memo(Chevron);

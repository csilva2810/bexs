import {
  validateNumber,
  validateName,
  validateValidThru,
  validateCVV,
  removeNumberMask,
  validateInstallments,
} from './utils';

describe('PaymentFormUtils', () => {
  describe('validateNumber', () => {
    it('should not return an error for valid numbers', () => {
      expect(validateNumber('1111222233334444')).toBe('');
    });

    it('should return an error for required', () => {
      expect(validateNumber('')).toBe('Campo obrigatório');
    });

    it('should return an error for invalid numbers', () => {
      expect(validateNumber('1112')).toBe('Número de cartão inválido');
      expect(validateNumber('1112A00012345555')).toBe(
        'Número de cartão inválido',
      );
    });
  });

  describe('validateName', () => {
    it('should not return an error for valid names', () => {
      expect(validateName('Carlos Alberto')).toBe('');
      expect(validateName('Carlos Alberto Silva Junior')).toBe('');
      expect(validateName('Carlos A S Junior')).toBe('');
    });

    it('should return an error for required', () => {
      expect(validateName('')).toBe('Campo obrigatório');
    });

    it('should return an error for incomplete names', () => {
      expect(validateName('carlos')).toBe('Insira seu nome completo');
    });
  });

  describe('validateValidThru', () => {
    it('should not return an error for valid dates', () => {
      expect(validateValidThru('01/21')).toBe('');
      expect(validateValidThru('06/20')).toBe('');
      expect(validateValidThru('10/20')).toBe('');
      expect(validateValidThru('12/20')).toBe('');
    });

    it('should return an error for required', () => {
      expect(validateValidThru()).toBe('Campo obrigatório');
    });

    it('should return an error for invalid dates', () => {
      expect(validateValidThru('13/20')).toBe('Data inválida');
      expect(validateValidThru('00/20')).toBe('Data inválida');
    });

    it('should return an error for expired dates', () => {
      expect(validateValidThru('01/19')).toBe('Cartão expirado');
      expect(validateValidThru('01/18')).toBe('Cartão expirado');
    });
  });

  describe('validateCVV', () => {
    it('should not return an error for valid CVV', () => {
      expect(validateCVV('123')).toBe('');
    });

    it('should return an error for required', () => {
      expect(validateCVV('')).toBe('Campo obrigatório');
    });

    it('should return an error for invalid CVV', () => {
      expect(validateCVV('a12')).toBe('Código inválido');
      expect(validateCVV('1')).toBe('Código inválido');
      expect(validateCVV('12')).toBe('Código inválido');
    });
  });

  describe('removeNumberMask', () => {
    it('should remove spaces from the credit card number', () => {
      expect(removeNumberMask('1234 1234 1234 1234')).toBe('1234123412341234');
    });
  });

  describe('validateInstallments', () => {
    it('should not return errors if a value is passed', () => {
      expect(validateInstallments('12')).toBe('');
    });

    it('should return an error for required', () => {
      expect(validateInstallments('')).toBe('Insira o número de parcelas');
    });
  });
});

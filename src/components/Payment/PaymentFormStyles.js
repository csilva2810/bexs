import styled from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

import { mediaQuery } from '../../styles/utils';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  ${mediaQuery(`
    flex-direction: row;
  `)}
`;

export const CvvHelper = styled.span`
  display: inline-flex;
  width: 1em;
  height: 1em;
  border-radius: 50%;
  background-color: ${themeGet('colors.border')};
  color: ${themeGet('colors.white')};
  margin-left: ${themeGet('space.2')}px;
  align-items: center;
  justify-content: center;
  font-size: 0.9em;
  font-style: italic;
  font-weight: bold;
`;

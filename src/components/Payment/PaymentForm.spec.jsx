/* eslint-disable import/first */
import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';

import PaymentForm from './PaymentForm';

jest.mock('../../services/payment');

import { sendPayment } from '../../services/payment';

describe('<PaymentForm />', () => {
  describe('validations', () => {
    test.each([
      ['Número do cartão', 'Campo obrigatório'],
      ['Nome (igual ao cartão)', 'Campo obrigatório'],
      ['Validade', 'Campo obrigatório'],
      ['CVV', 'Campo obrigatório'],
      ['Número de parcelas', 'Insira o número de parcelas'],
    ])('should validate "%s"', async (label, message) => {
      render(<PaymentForm />);
      const input = screen.getByLabelText(label);

      fireEvent.focus(input);
      fireEvent.blur(input);

      await waitFor(() =>
        expect(screen.getByText(message)).toBeInTheDocument(),
      );
    });
  });

  describe('interactions', () => {
    it('should flip the credit card when the user focus on the CVV input', async () => {
      render(<PaymentForm />);

      const input = screen.getByLabelText('CVV');
      const cvv = screen.getByTestId('card-cvv');

      fireEvent.focus(input);

      await waitFor(() => expect(cvv).toBeVisible());

      userEvent.type(input, '123');

      await waitFor(() => expect(cvv).toHaveTextContent('123'));
    });

    it('should show the user inputs on the credit card', async () => {
      render(<PaymentForm />);

      userEvent.type(
        screen.getByLabelText('Número do cartão'),
        '1111222233334444',
      );
      userEvent.type(
        screen.getByLabelText('Nome (igual ao cartão)'),
        'carlos a s junior',
      );
      userEvent.type(screen.getByLabelText('Validade'), '12/23');

      await waitFor(() => expect(screen.getByText('1111')).toBeInTheDocument());
      await waitFor(() => expect(screen.getByText('2222')).toBeInTheDocument());
      await waitFor(() => expect(screen.getByText('3333')).toBeInTheDocument());
      await waitFor(() => expect(screen.getByText('4444')).toBeInTheDocument());
      await waitFor(() =>
        expect(screen.getByText('carlos a s junior')).toBeInTheDocument(),
      );
      await waitFor(() =>
        expect(screen.getByText('12/23')).toBeInTheDocument(),
      );
    });

    it('should send the form information to the API', async () => {
      render(<PaymentForm />);

      const button = screen.getByRole('button', { name: 'Continuar' });

      userEvent.type(
        screen.getByLabelText('Número do cartão'),
        '1234567812345678',
      );
      userEvent.type(
        screen.getByLabelText('Nome (igual ao cartão)'),
        'carlos a s junior',
      );
      userEvent.type(screen.getByLabelText('Validade'), '12/23');
      userEvent.type(screen.getByLabelText('CVV'), '123');
      userEvent.selectOptions(screen.getByLabelText('Número de parcelas'), [
        '12',
      ]);

      userEvent.click(button);

      await waitFor(() => expect(button).toHaveTextContent('Aguarde...'));
      await waitFor(() =>
        expect(sendPayment).toHaveBeenCalledWith({
          number: '1234567812345678',
          name: 'carlos a s junior',
          validThru: '12/23',
          cvv: '123',
          installments: '12',
        }),
      );
      await waitFor(() => expect(button).toHaveTextContent('Continuar'));
    });
  });
});

import React, { useState, useCallback, memo, useMemo } from 'react';
import { useFormik } from 'formik';

import { sendPayment } from '../../services/payment';

import Box from '../ui/Box';
import Button from '../ui/Button';
import Breadcrumb from '../ui/Breadcrumb';
import { Input, Select } from '../ui/Input';

import Header from './Header';
import CreditCard from './CreditCard';

import * as Styles from './PaymentFormStyles';
import {
  validateNumber,
  validateValidThru,
  validateName,
  validateCVV,
  removeNumberMask,
  validateInstallments,
} from './utils';

const NUMBER_MASK = '9999 9999 9999 9999';
const VALID_TRHU_MASK = '99/99';
const CVV_MASK = '999';

const breadcrumbItems = [
  {
    label: 'Carrinho',
    isDone: true,
  },
  {
    label: 'Pagamento',
    isDone: false,
  },
  {
    label: 'Confirmação',
    isDone: false,
  },
];

const validate = (values) => {
  const errors = {};
  const validations = {
    number: validateNumber(removeNumberMask(values.number)),
    name: validateName(values.name),
    validThru: validateValidThru(values.validThru),
    cvv: validateCVV(values.cvv),
    installments: validateInstallments(values.installments),
  };

  Object.entries(validations).forEach(([key, error]) => {
    if (error) {
      errors[key] = error;
    }
  });

  return errors;
};

const PaymentForm = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [flipCard, setFlipCard] = useState(false);
  const formik = useFormik({
    initialValues: {
      number: '',
      name: '',
      validThru: '',
      cvv: '',
      installments: '',
    },
    validate,
    onSubmit: async (values) => {
      if (isLoading) {
        return;
      }

      setIsLoading(true);

      try {
        await sendPayment(values);
      } finally {
        setIsLoading(false);
      }
    },
  });
  const { number, name, validThru, cvv, installments } = formik.values;
  const { handleSubmit, handleBlur, handleChange, errors, touched } = formik;

  const getError = useCallback(
    (field) => {
      if (touched[field] && errors[field]) {
        return errors[field];
      }

      return '';
    },
    [errors, touched],
  );

  const onFocusCVV = useCallback(() => {
    setFlipCard(true);
  }, []);

  const onBlurCVV = useCallback(
    (e) => {
      handleBlur(e);
      setFlipCard(false);
    },
    [handleBlur],
  );

  const cvvLabel = useMemo(
    () => (
      <Box alignItems="center" id="cvv-label">
        CVV
        <Styles.CvvHelper title="Código de três dígitos atrás do cartão">
          i
        </Styles.CvvHelper>
      </Box>
    ),
    [],
  );

  const installmentItems = useMemo(
    () => (
      <>
        <option value=""></option>
        <option value="12">12x R$ 1.000,00 sem juros</option>
        <option value="11">11x R$ 1.100,00 sem juros</option>
        <option value="10">10x R$ 1.200,00 sem juros</option>
      </>
    ),
    [],
  );

  return (
    <Styles.Container>
      <Header>
        <CreditCard
          number={removeNumberMask(number)}
          name={name}
          validThru={validThru}
          cvv={cvv}
          isFlipped={flipCard}
        />
      </Header>

      <Box
        flexDirection="column"
        flex="1"
        width={['280px', 'auto']}
        height="100%"
        mx="auto"
        mt={['114px', '50px']}
        pl={[0, '168px']}
        pr={[0, '64px']}
      >
        <Box ml={[4, 'auto']} mb="70px" display={['none', 'block']}>
          <Breadcrumb items={breadcrumbItems} />
        </Box>

        <form onSubmit={handleSubmit}>
          <Box>
            <Input
              label="Número do cartão"
              type="tel"
              id="number"
              name="number"
              value={number}
              onChange={handleChange}
              maxLength={NUMBER_MASK.length}
              mask={NUMBER_MASK}
              onBlur={handleBlur}
              error={getError('number')}
            />
          </Box>

          <Box mt={[8, 10]}>
            <Input
              label="Nome (igual ao cartão)"
              type="text"
              id="name"
              name="name"
              value={name}
              onChange={handleChange}
              onBlur={handleBlur}
              error={getError('name')}
            />
          </Box>

          <Box mt={[8, 10]}>
            <Box mr={2} flex="1 1 100%">
              <Input
                label="Validade"
                type="tel"
                id="validThru"
                name="validThru"
                mask={VALID_TRHU_MASK}
                maxLength={VALID_TRHU_MASK.length}
                value={validThru}
                onChange={handleChange}
                onBlur={handleBlur}
                error={getError('validThru')}
              />
            </Box>

            <Box flex="1 1 100%">
              <Input
                aria-labelledby="cvv-label"
                label={cvvLabel}
                type="tel"
                id="cvv"
                name="cvv"
                mask={CVV_MASK}
                maxLength={CVV_MASK.length}
                value={cvv}
                onChange={handleChange}
                onFocus={onFocusCVV}
                onBlur={onBlurCVV}
                error={getError('cvv')}
              />
            </Box>
          </Box>

          <Box mt={[8, 10]}>
            <Select
              label="Número de parcelas"
              id="installments"
              name="installments"
              value={installments}
              onChange={handleChange}
              onBlur={handleBlur}
              error={getError('installments')}
              items={installmentItems}
            />
          </Box>

          <Box my={[8, 10]} width={[1, '60%']} ml="auto">
            <Button type="submit" fullWidth>
              {isLoading ? 'Aguarde...' : 'Continuar'}
            </Button>
          </Box>
        </form>
      </Box>
    </Styles.Container>
  );
};

export default memo(PaymentForm);

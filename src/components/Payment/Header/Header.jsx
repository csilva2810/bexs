import React, { memo } from 'react';

import Box from '../../ui/Box';
import Text from '../../ui/Text';
import Chevron from '../../ui/Chevron';

import * as Styles from './HeaderStyles';

const Header = ({ children }) => {
  return (
    <Styles.Header>
      <Styles.TitleContainer>
        <Styles.BackButton>
          <Chevron direction="left" size="100%" />
        </Styles.BackButton>
        <Styles.Title>
          <Text as="div" textAlign="center" variant="body2">
            <Box display={['none', 'block']}>Alterar forma de pagamento</Box>
            <Box display={['block', 'none']}>
              <strong>Etapa 2</strong> de 3
            </Box>
          </Text>
        </Styles.Title>
      </Styles.TitleContainer>

      {children}
    </Styles.Header>
  );
};

export default memo(Header);

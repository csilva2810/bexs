import styled from 'styled-components/macro';
import { themeGet } from '@styled-system/theme-get';

import { mediaQuery } from '../../../styles/utils';

export const Header = styled.header`
  height: 239px;
  padding-top: 38px;
  color: ${themeGet('colors.white')};
  background-color: ${themeGet('colors.primary')};

  ${mediaQuery(`
    height: 100%;
    width: 352px;
    padding-left: 64px;
    padding-top: 50px;
`)}
`;

export const TitleContainer = styled.div`
  ${mediaQuery(`
    display: flex;
    align-items: center;
  `)}
`;

export const BackButton = styled.button`
  position: absolute;
  top: 38px;
  left: 15px;
  width: 20px;

  ${mediaQuery(`
    position: static;
    width: 12px;
    height: auto;
    margin-right: 15px;
  `)}
`;

export const Title = styled.div`
  margin-top: 1px;
`;

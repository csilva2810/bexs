import React, { memo, useMemo } from 'react';

import Box from '../../ui/Box';
import Text from '../../ui/Text';
import UICreditCard from '../../ui/CreditCard';

import { ReactComponent as CreditCardIcon } from '../../../assets/credit-card.svg';
import logoVisa from '../../../assets/logo-visa@2x.png';

const fillWithStars = (string, length) => string.padEnd(length, '*');
const splitCardNumber = (number) => number.split(/(.{4})/).filter(Boolean);

const CreditCard = ({ number, name, validThru, cvv, isFlipped }) => {
  const numbers = useMemo(() => splitCardNumber(fillWithStars(number, 16)), [
    number,
  ]);
  const flag = useMemo(() => (/\d{4}/.test(numbers[0]) ? logoVisa : ''), [
    numbers,
  ]);

  return (
    <>
      <Box
        mx="auto"
        pr={[0, 2]}
        mt={[6, 10]}
        alignItems="center"
        maxWidth={['220px', '100%']}
      >
        <Box mr={3} width={['40px', '50px']}>
          <CreditCardIcon />
        </Box>
        <Text fontWeight="bold" fontSize={[0, 1]}>
          Adicione um novo cartão de crédito
        </Text>
      </Box>

      <Box mt={[3, 6]} mx="auto" width={['280px', '365px']}>
        <UICreditCard
          flag={flag}
          numbers={numbers}
          name={name}
          validThru={validThru}
          cvv={fillWithStars(cvv, 3)}
          isFlipped={isFlipped}
        />
      </Box>
    </>
  );
};

export default memo(CreditCard);

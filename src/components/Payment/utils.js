import isBefore from 'date-fns/isBefore';
import isValid from 'date-fns/isValid';

export const validateNumber = (value) => {
  if (!value) {
    return 'Campo obrigatório';
  }

  if (!/^\d{16}$/g.test(value)) {
    return 'Número de cartão inválido';
  }

  return '';
};

export const validateName = (value) => {
  if (!value) {
    return 'Campo obrigatório';
  }

  if (!/\w+\s\w+(\s\w+)*/gi.test(value)) {
    return 'Insira seu nome completo';
  }

  return '';
};

export const validateValidThru = (value) => {
  if (!value) {
    return 'Campo obrigatório';
  }

  const now = new Date();
  const [month, year] = value.split('/').map(Number);

  if (month < 1 || month > 12) {
    return 'Data inválida';
  }

  const millenium = String(now.getFullYear()).slice(0, 2);
  const validThruDate = new Date(`${millenium}${year}`, month - 1);

  if (!isValid(validThruDate)) {
    return 'Data inválida';
  }

  if (isBefore(validThruDate, now)) {
    return 'Cartão expirado';
  }

  return '';
};

export const validateCVV = (value) => {
  if (!value) {
    return 'Campo obrigatório';
  }

  if (!/^\d{3}$/g.test(value)) {
    return 'Código inválido';
  }

  return '';
};

export const removeNumberMask = (number) => number.replace(/\s/gi, '');

export const validateInstallments = (value) => {
  if (!value) {
    return 'Insira o número de parcelas';
  }

  return '';
};

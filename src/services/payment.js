import api from './api';

export const sendPayment = async (paymentForm) => {
  const response = await api({
    endpoint: '/pagar',
    method: 'POST',
    body: paymentForm,
  });

  return response;
};

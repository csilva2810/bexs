const BASE_URL = 'https://bexs.api.com.br/v1';

const defaultHeaders = {
  'Content-Type': 'application/json',
};

const api = ({ endpoint = '/', method = 'GET', headers = {}, body = {} }) => {
  const url = `${BASE_URL}${endpoint}`;
  const options = {
    method,
    headers: {
      ...defaultHeaders,
      ...headers,
    },
    body: JSON.stringify(body),
  };

  return fetch(url, options).then((response) => response.json());
};

export default api;
